{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-unused-local-binds #-}

module Site.Org.Render.Backend (backend) where

import Control.Monad.Logger (logWarnN)
import Data.Aeson qualified as Aeson
import Data.IxSet.Typed qualified as Ix
import Data.List qualified as L
import Data.Text qualified as T
import Data.Yaml qualified as Yaml
import Ema.Route.Url (routeUrl)
import Ondim.Extra.BindJSON (openObject)
import Ondim.Targets.HTML (HtmlNode (TextNode), fromNodeList)
import Org.Exporters.HTML (HtmlBackend, defHtmlBackend)
import Org.Types (LinkTarget (..), OrgElement (..), srcLinesToText)
import Site.Org.Model
import Site.Org.Render.Types
import Text.XmlHtml qualified as X

backend :: Pages -> RPrism -> HtmlBackend RenderT
backend p route = fix \self ->
  let def = defHtmlBackend
      m "o" [e] = callExpansion e (nullObj def)
      m _ _ = pure []
   in def
        { macro = m,
          customTarget = resolveTarget p route,
          customElement = customElementPipeline self
        }

customElementPipeline ::
  HtmlBackend RenderT -> OrgElement -> Maybe (Ondim [HtmlNode])
customElementPipeline bk x =
  asum $
    flap
      [ customSourceBlock bk
      ]
      x

customSourceBlock ::
  HtmlBackend RenderT -> OrgElement -> Maybe (Ondim [HtmlNode])
customSourceBlock bk = \case
  SrcBlock {..} -> do
    e <- L.lookup "expand" srcBlkArguments
    let content = encodeUtf8 $ srcLinesToText srcBlkLines
    if e == "t" && srcBlkLang == "html"
      then do
        parsed <- rightToMaybe $ X.parseHTML "" content
        return $ liftNodes $ fromNodeList $ X.docContent parsed
      else do
        contentObj :: Aeson.Object <-
          case srcBlkLang of
            "json" -> Aeson.decodeStrict content
            "yaml" -> either (error . show) id $ Yaml.decodeThrow content
            _ -> Nothing
        return $
          openObject @HtmlNode Nothing contentObj $
            bindKeywords bk "akw:" affKws $
              callExpansion e $
                TextNode ""
  _ -> Nothing

resolveTarget :: Pages -> RPrism -> LinkTarget -> Maybe (Ondim Text)
resolveTarget m rp = \case
  (URILink "id" id')
    | (id_, anchor) <- breakInternalRef id',
      Just page <- Ix.getOne (m Ix.@= OrgID id_) ->
        Just $ pure $ route (Route_Page $ _identifier page) <> anchor
  (URILink uri l)
    | (toString -> path, anchor) <- breakInternalRef l,
      Just sAlias <- T.stripPrefix "source:" uri,
      Just source <- readMaybe (toString sAlias) ->
        let ix_ = OrgPath source path
            orgRoute = do
              ident <- _identifier <$> Ix.getOne (m Ix.@= LevelIx 0 Ix.@= ix_)
              pure $ Route_Page ident
            staticRoute = do
              let sroute = StaticFileIx ix_
              guard $ not $ Ix.null (m Ix.@= sroute)
              pure $ Route_Static sroute
         in case orgRoute <|> staticRoute of
              Just finalRoute -> Just $ pure $ route finalRoute <> anchor
              Nothing -> Just do
                lift $ logWarnN $ "Broken link found: " <> prettyOrgPath ix_ <> " does not exist."
                return (uri <> "://" <> l)
  _ -> Nothing
  where
    breakInternalRef =
      second (\x -> maybe x ("#" <>) (T.stripPrefix "::" x)) . T.breakOn "::"
    route = routeUrl rp
