{-# LANGUAGE RankNTypes #-}

module Site.Org.PreProcess (DocLike (..), walkPreProcess, PreProcessEnv (..)) where

import Control.Monad.Logger (MonadLogger, logDebugN, logWarnNS)
import Optics.Core
import Org.Types
import Org.Walk
import Relude.Extra (lookup)
import Site.Org.Model
import Site.Org.Options (Options (..), Source (..))
import Site.Org.Utils.Document (isolateSection)
import System.FilePath (dropExtension, isAbsolute, isExtensionOf, isRelative, makeRelative, normalise, (</>))
import UnliftIO (MonadUnliftIO)
import UnliftIO.Directory (canonicalizePath, doesDirectoryExist, doesFileExist, makeAbsolute)

class DocLike a where
  getLevel :: a -> Int
  getTags :: a -> Tags
  getProps :: a -> Properties
  getTitle :: a -> Maybe [OrgObject]
  toDoc :: a -> OrgDocument

instance DocLike OrgDocument where
  getLevel _ = 0
  getTags _ = []
  getProps = documentProperties
  getTitle _ = Nothing
  toDoc = id

instance DocLike OrgSection where
  getLevel = sectionLevel
  getTags = sectionTags
  getProps = sectionProperties
  getTitle = Just . sectionTitle
  toDoc = isolateSection

data PreProcessEnv = PreProcessEnv
  { -- Constant
    sources :: [Source],
    path :: OrgPath,
    srcDir :: FilePath,
    relDir :: FilePath,
    opts :: Options,
    -- Inherited
    attachDir :: Maybe FilePath,
    inhProps :: Properties
  }
  deriving (Generic)

type PreProcessM m = ReaderT PreProcessEnv m

walkPreProcess :: (MonadUnliftIO m, MonadLogger m) => WalkM (PreProcessM m)
walkPreProcess = buildMultiW \f l ->
  l
    .> processLink f
    .> processEntry @OrgSection f
    .> processEntry @OrgDocument f

-- | Resolve links to other files
processLink :: (MonadUnliftIO m, MonadLogger m) => WalkM (PreProcessM m) -> OrgObject -> PreProcessM m OrgObject
processLink f = \case
  (Link t c) -> Link <$> processTarget t <*> mapM f c
  l -> f l

processTarget :: forall m. (MonadUnliftIO m, MonadLogger m) => LinkTarget -> PreProcessM m LinkTarget
processTarget t = do
  env <- ask
  case t of
    -- Link to another file.
    l@(URILink "file" (toString -> fp)) ->
      fromMaybe l <$> findTarget fp
    l@(URILink "attachment" att)
      | Just aDir <- attachDir env ->
          fromMaybe l <$> findTarget (aDir </> toString att)
    l -> pure l
  where
    findTarget :: FilePath -> PreProcessM m (Maybe LinkTarget)
    findTarget fp = do
      src <- findSource fp
      return do
        oP <- src
        let uri = "source:" <> show (opSource oP)
            target =
              toText $
                if "org" `isExtensionOf` opPath oP
                  then dropExtension $ opPath oP
                  else opPath oP
        return $ URILink uri target

processEntry :: (DocLike a, MonadLogger m, MonadUnliftIO m, MultiWalk MWTag a) => WalkM (PreProcessM m) -> a -> PreProcessM m a
processEntry f s = do
  env <- ask

  let sectionId = lookup "id" (getProps s)

      updateId = case sectionId of
        Just x | sectionId /= lookup "id" (inhProps env) -> Just x
        _ -> Nothing

  newAttachDir <- join <$> forM updateId getAttachDir
  lift $ whenJust newAttachDir $ logDebugN . ("New attach dir: " <>) . show

  let childEnv _ =
        env
          & #inhProps %~ (<> getProps s)
          & #attachDir %~ maybe id (const . pure) newAttachDir

  local childEnv $ f s

findSource :: (MonadIO m, MonadLogger m) => FilePath -> PreProcessM m (Maybe OrgPath)
findSource fp = do
  env <- ask
  trueFp <-
    canonicalizePath
      if isAbsolute fp
        then fp
        else srcDir env </> relDir env </> fp

  unlessM (doesFileExist trueFp) $
    lift $
      logWarnNS "findSource" $
        "File " <> show trueFp <> " linked from " <> prettyOrgPath (path env) <> " does not exist."

  op <-
    asum <$> forM (sources env) \source -> do
      absSrc <- canonicalizePath (dir source)
      let mbRel = normalise $ makeRelative absSrc trueFp
      return $
        if isRelative mbRel
          then Just (OrgPath source mbRel)
          else Nothing

  whenNothing_ op $
    lift $
      logWarnNS "findSource" $
        "Could not find a source for " <> show fp <> " at " <> prettyOrgPath (path env) <> "."

  pure op

getAttachDir :: MonadIO m => Text -> PreProcessM m (Maybe FilePath)
getAttachDir key = do
  env <- ask
  let rid = toString key
      orgAttachIdTSFolderFormat = take 6 rid </> drop 6 rid
      orgAttachIdUUIDFolderFormat = take 2 rid </> drop 2 rid

  possibleDirs <-
    mapM makeAbsolute $
      (srcDir env </>) . (orgAttachDir (opts env) </>)
        <$> [ orgAttachIdTSFolderFormat,
              orgAttachIdUUIDFolderFormat,
              rid
            ]

  findM doesDirectoryExist possibleDirs
  where
    findM p = foldr (\x -> ifM (p x) (pure $ Just x)) (pure Nothing)
