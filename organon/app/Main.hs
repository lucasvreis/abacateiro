module Main where

import Data.Yaml qualified as Y
import Ema.App (runSite_)
import Site.Organon.Config qualified as Config
import Site.Organon.Route
import UnliftIO (catch)
import UnliftIO.Directory (setCurrentDirectory)

main :: IO ()
main = do
  setCurrentDirectory "/home/lucas/dados/projetos/sites/gatil"
  cfg <-
    Config.loadConfigWithDefault "organon.yaml"
      `catch` (error . toText . Y.prettyPrintParseException)
  runSite_ @Route cfg
