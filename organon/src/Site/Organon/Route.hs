{-# LANGUAGE UndecidableInstances #-}

module Site.Organon.Route where

import Data.Generics.Sum.Any
import Data.List qualified as L
import Data.Map (keys, (!?))
import Ema
import Ema.Route.Generic
import Ema.Route.Lib.Extra.StaticRoute qualified as SR
import Generics.SOP qualified as SOP
import Ondim.Extra (getAttrs)
import Ondim.Targets.HTML (HtmlNode, HtmlTag)
import Optics.Core
import Site.Org ()
import Site.Org.Model qualified as O
import Site.Org.Render
import Site.Organon.Config qualified as Config
import Site.Organon.Dynamic
import Site.Organon.Extra.LaTeX (renderLaTeXExp)
import Site.Organon.Extra.Query (queryExp)
import Site.Organon.Model (Model (..))

data Route
  = RouteStatic (SR.StaticRoute "assets")
  | RouteContent O.Route
  deriving (Eq, Show, Generic, SOP.Generic, SOP.HasDatatypeInfo)
  deriving
    (HasSubRoutes, HasSubModels, IsRoute)
    via ( GenericRoute
            Route
            '[ WithSubRoutes
                 '[ FolderRoute "assets" (SR.StaticRoute "assets"),
                    O.Route
                  ],
               WithModel Model
             ]
        )

instance EmaSite Route where
  type SiteArg Route = Config.Config
  siteInput act cfg = do
    dR <- siteInput @O.Route act (Config.orgFiles cfg)
    dS <- siteInput @(SR.StaticRoute "assets") act ()
    dO <- ondimDynamic (Config.templates cfg)
    dL <- layoutDynamic (Config.layouts cfg)
    dC <- cacheDynamic (Config.cacheFile cfg)
    return $ Model <$> dR <*> dS <*> dO <*> dL <*> dC ?? Config.extraOptions cfg

  siteOutput rp model route =
    case route of
      RouteContent r -> ondimOutput (rp % _As @O.Route) orgM r
      RouteStatic r -> siteOutput (rp % _As @"RouteStatic") (staticM model) r
    where
      ondimOutput ::
        (RenderM m, SiteOutput r ~ OndimOutput, EmaSite r) =>
        Prism' FilePath r ->
        (Model -> RouteModel r) ->
        r ->
        m (SiteOutput Route)
      ondimOutput p s r =
        renderWithLayout =<< siteOutput p (s model) r

      renderWithLayout :: RenderM m => OndimOutput -> m (Asset LByteString)
      renderWithLayout = \case
        AssetOutput x -> run x
        PageOutput lname doc
          | Just layout <- (model ^. #layouts) !? lname -> AssetGenerated Html <$> run (doc layout)
          | otherwise -> error $ "Could not find layout " <> lname
        where
          run :: OutputT m a -> m a
          run x = runReaderT x (OutputEnv ostate)
          files = keys $ model ^. (#staticM % #modelFiles)
          encExps =
            -- TODO this is bad
            files <&> \file ->
              ( "asset:" <> toText file,
                pure $
                  SR.staticRouteUrl
                    (rp % _As @"RouteStatic")
                    (model ^. #staticM)
                    file
              )
          extraExps =
            [ ("query", queryExp (rp % _As @O.Route) (orgM model)),
              ("organon:latex", renderLaTeXExp model),
              ("unwrap", unwrapExp),
              ("portal", portalExp),
              ("o:parse", parseObjectsExp (backend (orgM model ^. #_mPages) (rp % _As @O.Route)))
            ]
          extraFilters =
            [ ("target", targetFilter)
            ]
          ostate :: OndimMS =
            model ^. #ondimS
              & lensVL ondimGState
                %~ (#textExpansions %~ (fromList encExps <>))
              & lensVL ondimState
                %~ (#expansions %~ (fromList extraExps <>))
                  . (#filters %~ (fromList extraFilters <>))

targetFilter :: Filter HtmlNode
targetFilter thunk = do
  p <- sequence =<< getTextExpansion "portal-target"
  nodes <- thunk
  forM nodes \node -> do
    let id_ = L.lookup "id" $ getAttrs @HtmlTag node
    if isJust p && p == id_
      then returnEarly (children @HtmlTag node)
      else return node

portalExp :: Expansion HtmlNode
portalExp node =
  either id id
    <$> createPortal (liftChildren node)
