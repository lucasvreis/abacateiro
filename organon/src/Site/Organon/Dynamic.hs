module Site.Organon.Dynamic where

import Control.Monad.Logger (MonadLogger, logErrorNS, logInfoNS)
import Data.Binary (encodeFile)
import Data.Map (singleton)
import Ema.Dynamic (Dynamic (..))
import Ondim.Targets.HTML.Load (loadTemplatesDynamic)
import Org.Exporters.HTML (htmlTemplateDir)
import Site.Org.Render.Types (Layouts, OndimMS)
import Site.Organon.Cache (Cache, loadCache)
import System.FilePath (takeBaseName, (</>))
import System.UnionMount qualified as UM
import Text.XmlHtml qualified as X
import UnliftIO (MonadUnliftIO, finally)
import UnliftIO.Concurrent (threadDelay)

layoutDynamic :: (MonadUnliftIO m, MonadLogger m) => FilePath -> m (Dynamic m Layouts)
layoutDynamic dir = do
  Dynamic
    <$> UM.mount dir [((), "**/*.html")] [] mempty \() fp _fa ->
      X.parseHTML fp <$> readFileBS (dir </> fp) >>= \case
        Left e -> logErrorNS "Template Loading" (toText e) >> liftIO (fail e)
        Right tpl -> do
          let name = fromString $ takeBaseName fp
          pure (singleton name tpl <>)

ondimDynamic :: (MonadUnliftIO m, MonadLogger m) => FilePath -> m (Dynamic m OndimMS)
ondimDynamic dir = do
  ddir <- liftIO htmlTemplateDir
  Dynamic <$> loadTemplatesDynamic [dir, ddir]

cacheDynamic :: (MonadUnliftIO m, MonadLogger m) => FilePath -> m (Dynamic m (TVar Cache))
cacheDynamic file = do
  cVar <- loadCache file
  let close _ =
        forever (threadDelay maxBound) `finally` do
          logInfoNS "Organon" "Writing cache to disk."
          cache <- readTVarIO cVar
          liftIO $ encodeFile file cache
  pure $ Dynamic (cVar, close)
